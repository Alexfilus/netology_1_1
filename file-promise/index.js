const fs = require('fs');
module.exports = {
    read : (filepath) => {
        return new Promise((resolve, reject) => {
            fs.readFile(filepath, (err, buffer) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(buffer.toString());
            });
        });
    },

    write : (filepath, data) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(filepath, data, (err) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(filepath);
            });
        });
    }
};