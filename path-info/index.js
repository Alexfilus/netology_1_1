const fs = require('fs');
module.exports = (path, callback) => {
    fs.stat(path, function(err, stats) {
        let info = {};
        info.path = path;
        if (stats.isFile()) {
            info.type = 'file';
            fs.readFile(path, {encoding: 'utf8'}, (err, buffer) => {
                info.content = buffer.toString();
                callback(err, info);
            });
        }
        if (stats.isDirectory()) {
            info.type = 'directory';
            fs.readdir(path, (err, items) => {
                info.childs = items;
                callback(err, info);
            });
        }
    });
};