const fs = require('fs');
const file = require('../file-promise');
module.exports = (path) => {
    return new Promise((resolve, reject) => {
        fs.readdir(path, (err, items) => {
            if (err) {
                reject(err);
                return;
            }
            let res = [];
            Promise.all([...Array(items.length).keys()].map((i) => {
                return file.read(path + items[i])
            })).then(data => data.forEach((data, index) => {
                res.push(
                    {
                        'name': items[index],
                        'content': data
                    }
                )
                resolve(res);
            }));
        });
    });
};